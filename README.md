
<h1>MON PORTFOLIO</h1>
<h3>Dans le cadre de ma formation de développeur Web chez Simplon, je dois réaliser mon Portfolio</h3>

- PROJET : [Portfolio](https://kamel-ben-abdelkader.gitlab.io/my-portfolio.gi/)

Processus du Projet : J'ai réaliser une maquette, ensuite j'ai realiser ma structure HTML, puis j'ai travailler sur le CSS.

-----------------

<h3>Maquette réalisée</h3>

<img src="images/maquette-portfolio/Home.jpg" width="300" height="300" />
<img src="images/maquette-portfolio/about.jpg" width="300" height="300" />
<img src="images/maquette-portfolio/Mes Projets.jpg" width="300" height="300" />
<img src="images/maquette-portfolio/Mes competences.jpg" width="300" height="300" />
<img src="images/maquette-portfolio/Mon gitlab.jpg" width="300" height="300" />
<img src="images/maquette-portfolio/contact et footer.jpg" width="300" height="300" />



-----------------

- 👨‍💻 Tous mes projets en cours sont disponibles sur mon GITLAB : [https://gitlab.com/Kamel.benabdelkader](https://gitlab.com/Kamel.benabdelkader)


-----------------

<h3>Me contacter:</h3>
<p align="left">
<a href="https://linkedin.com/in/https://www.linkedin.com/in/kamel-ben-abdelkader-094928167/" target="blank"><img align="center" src="images/maquette-portfolio/linkedin.png" alt="https://www.linkedin.com/in/kamel-ben-abdelkader-094928167/" height="30" width="40" /></a>
</p>

-----------------

<h3>Language et outil:</h3>
<p align="left"> <a href="https://getbootstrap.com" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/bootstrap/bootstrap-plain-wordmark.svg" alt="bootstrap" width="40" height="40"/> </a> <a href="https://www.w3schools.com/css/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="40" height="40"/> </a> <a href="https://www.w3.org/html/" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="40" height="40"/> </a> <a href="https://www.photoshop.com/en" target="_blank"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/photoshop/photoshop-line.svg" alt="photoshop" width="40" height="40"/> </a> </p>
